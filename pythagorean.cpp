#include <iostream>
#include <cmath>

/*
  PYTHAGOREAN
  Fonction principale 
  */
int main() {

  float cote_a;
  float cote_b;
  
  // Obtenir les valeurs de A et B
  std::cout << "Théorème de Pythagore" << std::endl;
  std::cout << "Entrez une valeur pour le côté A : " << std::endl;
  std::cin >> cote_a;
  std::cout << "Entrez une valeur pour le côté B : " << std::endl;
  std::cin >> cote_b;

  // Calcul du côté C
  float cote_c = sqrt(cote_a*cote_a + cote_b*cote_b);

  // Affichage du résultat
  std::cout << "La longueur du côté C est : " << std::endl;
  std::cout << cote_c << std::endl;

  return 0;
}
