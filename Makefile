
pythagorean: pythagorean.o
	g++ pythagorean.o -o pythagorean

pythagorean.o: pythagorean.cpp
	g++ -c pythagorean.cpp

test: pythagorean
	./pythagorean
